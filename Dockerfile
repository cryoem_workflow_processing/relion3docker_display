ARG cuda_version=9.1
ARG cuda_env=devel
ARG os=centos7

FROM nvidia/cuda:${cuda_version}-${cuda_env}-${os}

RUN yum update -y && \
    yum install -y cmake make gcc gcc-c++ git wget patch fftw3-devel libtiff-devel libXmu-devel csh ghostscript 

RUN mkdir /opt/openmpi && cd /opt/openmpi/ &&  wget https://download.open-mpi.org/release/open-mpi/v4.0/openmpi-4.0.2.tar.gz && \
tar -xvf openmpi-4.0.2.tar.gz && cd openmpi-4.0.2 &&  mkdir build && ./configure && make && make install

ENV OMPI_MCA_blt=self,sm,tcp
ENV OMPI_MCA_btl_vader_single_copy_mechanism=none
ENV OMPI_ALLOW_RUN_AS_ROOT=1
ENV OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1

# RUN yum install xorg-x11-drivers xorg-x11-server-Xorg xorg-x11-utils xorg-x11-xauth xorg-x11-xinit libXmu-devel -y

RUN mkdir -p /opt/relion3 && cd /opt/relion3/ && git clone https://github.com/3dem/relion.git . && mkdir build && \
    cmake -DCMAKE_INSTALL_PREFIX=/usr/local && make && make install && rm -rf /opt/relion3

RUN cd /etc/yum.repos.d && \
    curl -O https://winswitch.org/downloads/CentOS/winswitch.repo && \
    yum repolist && \
    yum install xpra -y

RUN mkdir -p /opt/ctffind && cd /opt/ctffind && \
curl "https://grigoriefflab.umassmed.edu/system/tdf?path=ctffind-4.1.13-linux64.tar.gz&file=1&type=node&id=26" -o ctffind.tar.gz && \
tar -xvf ctffind.tar.gz && mv bin/* /usr/local/bin/
ENV RELION_CTFFIND_EXECUTABLE=/usr/local/bin/ctffind

RUN curl "https://www.mrc-lmb.cam.ac.uk/kzhang/Gctf/Gctf_v1.18_b2/bin/Gctf_v1.18_b2_sm30_cu9.1" -o /usr/local/bin/gctf
ENV RELION_GCTF_EXECUTABLE=/usr/local/bin/gctf

RUN curl https://github.com/FredHutch/ls2_relion/raw/master/MotionCor2_1.1.0-Cuda91 -o /usr/local/bin/motioncor2
ENV RELION_MOTIONCOR2_EXECUTABLE=/usr/local/bin/motioncor2

ENV LD_LIBRARY_PATH=/usr/local/lib64:/usr/local/cuda/lib64:/usr/local/nvidia/lib64

WORKDIR /work

RUN adduser user && mkdir -p /run/user/1000/xpra && chown user /run/user/1000/xpra
USER user
