# Docker container for RELION 3.0

Run relion:
```
docker run -it --user $(id -u):$(id -g) --gpus all -v `pwd`:/work sbobkov/relion3 relion_executable
```

`relion_executable` - relion executable command with paramenters

Run display:
```
docker run -it --user $(id -u):$(id -g) -p 10000:10000 -v `pwd`:/work sbobkov/relion3 xpra start \
--bind-tcp=0.0.0.0:10000 --daemon=no --exit-with-children --start-child="relion_display"
```

`relion_display` -- relion_display command with input arguments

After launching the container, you can connect to http://localhost:10000 with a web-browser and find relion_display window.
If you close relion_display window by pressing X button, container will finish execution.
